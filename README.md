# 1‱, le jeu des ultra-riches !

Enthousiasmé et frustré par le jeu [Kapital](https://www.lavillebrule.com/catalogue/kapital-,130), voici le jeu **1‱**.
Un jeu pour découvrir les mécanismes de reproduction des inégalités qui enrayent le mythe du mérite, de l'ascensseur social, du ruissellement... sans sacrifier le gameplay sur l'autel de la pédagogie.

## Mots-clefs
Riche, ultra-riche, dominant, dominé, privilège, héritage, capital, capitalisme, symbolique, social, culturel, financier, plafond de verre, barrière invisible, ruissellement, mérite, ascensseur social, roturier, noble, privilégié, oppression, opprimé, intersectionnalité, Bourdieu, Pinçon-Charlot, sociologie, sociologue, lutte des classes, caste, classe social, prolétaire, bourgeois, possédant, exploitant, exploité, paradis fiscal, évasion fiscale, optimisation fiscale, niche fiscale.
