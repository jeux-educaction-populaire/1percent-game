interface Card {
    readonly description: string;
    readonly quiz: Quiz;
    readonly revealKnowledgeThreshold: CulturalCapital;
    readonly branchCases: object;//FIXME
    readonly comment: string;
}

interface Quiz {
    readonly question: string;
    readonly goodAnswers: Array<string>;
    readonly badAnswers: string[];
    readonly minGoodToPass: number; // default 1
    readonly maxBadToPass: number; // default 0
    readonly chooseFrom: "all" | "userInput" | number; // default "all".
    // If Number, pick minGoodToPass random answer from goodAnswers
    // and fill up to number with random Answer from both unused pool.
}

type SocialCapital = number;
type CulturalCapital = number;
type SymbolicCapital = number;
type FinancialCapital = number;

type CapitalThreshold = SocialCapital | CulturalCapital | SymbolicCapital | FinancialCapital;
type CapitalChange = SocialCapital | CulturalCapital | SymbolicCapital | FinancialCapital;
//type CapitalTransfer = rules;

type Capital = SocialCapital | CulturalCapital | SymbolicCapital | FinancialCapital;
